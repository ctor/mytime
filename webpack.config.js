'use strict'

module.exports = {
  context: __dirname,
  entry: './www/assets/js/app.js',
  output: {
    path: __dirname + '/www',
    filename: 'index.js'
  }
};
