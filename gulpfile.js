var gulp        = require('gulp');
var mocha       = require('gulp-mocha');
var webpack     = require('gulp-webpack');

var proc        = require('child_process');
var browserSync = require('browser-sync');

/* Server tasks*/
var s, tasks = [];
// if server already started, test iterations.
if (s) { tasks.push('test'); }
gulp.task('serve', tasks, function () {
  if (s) {
    console.log('srv changed. reloading server.');
    s.kill();
  }
  s = proc.fork(__dirname+'/srv/index.js', [], {silent:true});
  s.stdout.pipe(process.stdout);
  s.stderr.pipe(process.stderr);
  process.on('exit', function () { s.kill(); });
});
gulp.task('test', ['serve'], function() {
  return gulp.src('test/index.js', {read: false})
    .pipe(mocha({reporter: 'progress'}));
});

/* Client tasks */
gulp.task('browser-sync', ['serve'], function() {
  browserSync.init(null, {
    open: false,
    port: 3001,
    proxy: {
      host: 'http://localhost',
    }
  });
});
gulp.task('webpack', function() {
  return gulp.src('www/assets/js/app.js')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('www/'))
    .pipe(browserSync.reload({stream: true, reloadDelay: 2000}));
});

/* Gulp tasks */
gulp.task('watch', function () {
  gulp.watch(['srv/**/*'], ['serve','test']);
  gulp.watch(['www/assets/**/*'], ['webpack']);
});


/*
  Set up gulp with auto-reloading.
  Note: Does not detect default task changes.
*/
gulp.task('reload:gulp', ['serve','browser-sync','watch']);
gulp.task('default', function () {
  var p = proc.spawn('./node_modules/.bin/gulp', ['reload:gulp'], {stdio: 'inherit'});
  gulp.watch(['gulpfile.js']).on('change', function (e) {
    console.log('gulpfile change. reloading gulp.');
    if (p) {
      p.kill();
      console.log('Killing prior gulp runtime.');
    }
    // spawn a child gulp process w/ parent stdio
    p = proc.spawn('./node_modules/.bin/gulp', ['reload:gulp'], {stdio: 'inherit'});
    console.log(new Date().getTime() + ': gulp restarted');
  });
});
