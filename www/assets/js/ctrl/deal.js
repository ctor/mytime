var m = require('moment');
var h = require('haversine');

module.exports = function controller ($scope, dealData) {
  var processData = function (data) {
    data.forEach(function (obj, i) {
      // appointment
      var next = data[i].next_appointment_times[0];
      data[i].next_appt = m(next).format('l');

      // destination
      var lat1 = parseFloat(data[i].location.lat);
      var lon1 = parseFloat(data[i].location.lon);
      // hardcoded startpoint
      var lat2 = parseFloat('34.052200');
      var lon2 = parseFloat('-118.242800');

      // distance
      var end =   { latitude: lat1, longitude: lon1 };
      var start = { latitude: lat2, longitude: lon2  };
      data[i].distance = h(start, end, {unit: 'mi'}).toFixed(2);
    });
    $scope.deals = data;
  };
  dealData.get(processData);
};
