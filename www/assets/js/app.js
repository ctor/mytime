var angular = require('./lib/angular');
require('angular-route');
require('angular-resource');

require('./services.js')

var myTime = angular
  // Module
  .module('myTime', ['ngRoute','dataServices'])

  // Controllers
  .controller('DealCtrl', require('./ctrl/deal'))

  // Routing
  .config(require('./router'))

