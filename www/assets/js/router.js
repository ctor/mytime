module.exports = function ($routeProvider) {
  // TODO: look into on-the-fly loading controllers instead of bloating the
  //       main index file. http://weblogs.asp.net/dwahlin/dynamically-loading-c
  //       ontrollers-and-views-with-angularjs-and-requirejs
	$routeProvider
		.when('/', {
			templateUrl: 'deal.htm',
			controller: 'DealCtrl'
		})
		.otherwise({ redirectTo: '/' });
};
