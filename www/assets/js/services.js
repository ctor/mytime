var dataServices = angular.module('dataServices', ['ngResource']);

/**
 * Register data services here for backend api and
 * passing data to and from the controller.
 */
dataServices
	.factory('dealData', function ($resource) {
		return $resource('/deals', {}, {
		  get: { method:'GET', isArray: true }
		});
	})
