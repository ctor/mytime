#!/usr/bin/node

// dependencies
var request = require('request');
var express = require('express');
var app = express();

// static files
app.use(express.static(__dirname + '/../www'));

// BYPASS FOR ERROR. TODO: CLEAN LATER.
//XMLHttpRequest cannot load 
//http://www.mytime.com/api/v1/deals.json?what=Massage&
//when=Anytime&where=34.052200,-118.242800. 
//No 'Access-Control-Allow-Origin' header is present on the requested 
//resource. Origin 'http://localhost:3000' is therefore not allowed access.
var cache;
var dealWithIt = function (res) {
  return function (error, response, body) {
    if (error || response.statusCode !== 200) {
      return;
    }

    cache = JSON.parse(body);
    res.send(cache);
  }
};
var getDeals = function (url) {
  return function (req, res) {
    request(url, dealWithIt(res));
  }
};
var url = 'http://www.mytime.com/api/v1/deals.json?'+
  'what=Massage&when=Anytime&where=34.052200,-118.242800';
app.get('/deals', getDeals(url));

// server
app.set('port', (process.env.PORT || 3000));
app.listen(app.get('port'), function() {
  console.log('Application has started. See localhost:' + app.get('port'));
});
